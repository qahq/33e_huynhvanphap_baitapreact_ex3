import logo from './logo.svg';
import './App.css';
import ShoesStore from './Ex3/ShoesStore';

function App() {
  return (
    <div className="App">
      <ShoesStore/>
    </div>
  );
}

export default App;
