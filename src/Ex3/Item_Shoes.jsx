import React, { Component } from "react";

export default class Item_Shoes extends Component {
  render() {
    //Cú pháp detructuring
    let { image, name } = this.props.data;
    return (
      <div className="col-3 mt-5 ">
        <div className="card h-100" style={{ width: "100%" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-warning"
            >
              Add to card
            </button>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
              className="ml-3 btn btn-secondary"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
