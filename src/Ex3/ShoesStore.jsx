import React, { Component } from 'react'
import { data_shoes } from './dataShoes'
import Item_Shoes from './Item_Shoes'
import Cart from './Cart';
import Detail_Shoes from './Detail_Shoes';

export default class ShoesStore extends Component {
  state = {
    shoes: data_shoes,
    cart: [],
    detailShoes : data_shoes[0],
  };
  renderContent = () => {
    return this.state.shoes.map((item) => {
      return <Item_Shoes handleChangeDetail = {this.handleChangeDetail}  handleAddToCart={this.handleAddToCart} data={item} />;
    });
  };
  handleAddToCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneCart = [...this.state.cart];

    //TH1: Trong giỏ hàng chưa có sản phẩm
    if (index == -1) {
      let newSp = { ...shoe, soLuong: 1 };
      cloneCart.push(newSp);
    } else {
      //TH2: Trong giỏ hàng đã có sản phẩm
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.cart.findIndex((shoe) => {
      return shoe.id == idShoe;
    });
    if (index == -1) return;
    let cloneCart = [...this.state.cart];
    cloneCart[index].soLuong = cloneCart[index].soLuong + value;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  handleChangeDetail=(shoe)=>{
    this.setState({
      detailShoes : shoe ,
    })
  }

  render() {
    return (
      <div className="container py-5">
        {this.state.cart.length > 0 && (
          <Cart 
            handleChangeQuantity={this.handleChangeQuantity}
            cart={this.state.cart}
          />
          )}
          <Detail_Shoes detail={this.state.detailShoes}/>
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
  
