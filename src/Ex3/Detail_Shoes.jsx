import React, { Component } from 'react'

export default class  extends Component {
  render() {
    return (
      <div className='row mt-5 p-5 border border-secondary'>
        <div className="col-3"><img src={this.props.detail.image} className="w-100" alt="" /></div>
        <div className="col-9">
            <p> Tên sản phẩm : {this.props.detail.name} </p>
            <p> Alias : {this.props.detail.alias}</p>
            <p> Price : {this.props.detail.price}</p>
            <p> Description : {this.props.detail.description}</p>
            <p> ShortDescription : {this.props.detail.shortDescription}</p>
            <p> Quantity : {this.props.detail.quantity}</p>
        </div>
      </div>
    )
  }
}

